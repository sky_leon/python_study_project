"""阻塞式进程池"""
"""区别：
    (1)添加一个执行一个，新的任务执行必须等待当前的任务完成；
"""

from multiprocessing import Pool
import os
import time
from random import random
 
 
def task(task_name):
    print("开始做任务了！", task_name)
    start = time.time()
    # 休眠
    time.sleep(random())
    end = time.time()
    print("完成任务：{}!用时：{},进程id：{}".format(task_name, (end - start), os.getpid()))
 
 
if __name__ == '__main__':
    pool = Pool(5)
    tasks = ["python1", "python2", "python3", "python4", "python5", "python6", "python7", "python8"]
    for task1 in tasks:
        pool.apply(task, args=(task1,))
    pool.close()
    pool.join()
    print("结束！！！")
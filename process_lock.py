#互斥锁完成进程间的通信同步。

import time
import multiprocessing as mp
from typing import Counter

lock = mp.Lock()       #创建进程互斥锁
Counter = mp.Value('i' ,0)  #使用共享内存作计数器

def hello(lock , Counter):
    """进程函数

    Args:
        lock ([type]): [description]
        Counter ([type]): [description]
    """
    if lock.acquire():       #请求互斥锁，如果被占用，则阻塞，直至获取到锁
        time.sleep(2)    #假装思考
        Counter.value += 1 
        print('我是第%d个'%Counter.value)
        
    lock.release()     #释放互斥锁，否则占用大量资源

def demo():
    p_list = list()
    for i in range(30):       #30个人计数
        p_list.append(mp.Process(target=hello ,args=(lock ,Counter)))
        p_list[-1].start()
        
    for t in p_list:
        t.join()
        
    print('统计完毕共有%d个人！'%Counter.value)
    
if __name__ =='__main__':
    demo()
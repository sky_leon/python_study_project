import numpy as np
import h5py

if __name__=='__main__':
    lats , lons =np.mgrid[-90:90:361j , -180:180:721j]
    temp = np.random.randint(100 , 300 ,lons.shape)
    print(lons.shape , lats.shape , temp.shape)
    fp = h5py.File(r'D:\python_project\data\hdf5demo.hdf5','w')
    
    lons_dataset = fp.create_dataset('lons',data = lons)         #写入经度数据集
    lats_dataset = fp.create_dataset('lats',data = lats)         #写入纬度数据集
    temp_dataset = fp.create_dataset('temp',data = temp)         #写入温度数据集
    
    lons_dataset.attrs['lons_range'] = [-180 , 180]      #写入经度属性
    lats_dataset.attrs['lats_range'] = [-90 , 90]        #写入纬度属性
    temp_dataset.attrs['temp_range'] = [100 , 300]       #写入温度属性
    
    fp.close()
    
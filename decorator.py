import time
def timer(func):
    def wrapper(*args , **kwds):
        t0 = time.time()
        func(*args , **kwds)
        t1 = time.time()
        print('耗时%0.3f秒'%(t1-t0))
    return wrapper

@timer
def do_something(delay):
    print('函数do_something开始！')
    time.sleep(delay)
    print('函数do_something结束！')

if __name__=='__main__':
    do_something(3)
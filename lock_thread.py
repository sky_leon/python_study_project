import time
import threading

lock = threading.Lock()     #创建互斥锁
counter = 0                  #计数器

def Hello():
    """线程函数
    """
    global counter         #全局变量
    
    if lock.acquire():          #请求互斥锁，如果被占用，则阻塞直至获取到锁
        time.sleep(0.2)     #假装思考，按键盘，需要0.2秒
        counter += 1
        print('我是第%d个'%counter)
        
    lock.release()       #释放互斥锁，否则很严重，造成资源浪费。
    
    
def demo():
    threads = list()
    for i in range(30):       #假设群里有30个人都喜欢使用python
        threads.append(threading.Thread(target=Hello))      #将线程函数分配给线程完成任务。
        threads[-1].start()
        
    for t in threads:            #迭代threads列表
        t.join()
        
    print('统计完毕，共有%d个人'%counter)
    
if __name__ =='__main__':
    demo()
import time
import threading


def hello(name):
    """线程函数：
    创建线程前，需要把交给线程去做的工作写成一个函数，这个函数叫做线程函数。

    Args:
        name ([string]): [description]
    """
    
    for i in range(5):
        time.sleep(1)
        print('Hello , 我是%s'%name)
        
def demo():
    A = threading.Thread(target=hello , args = ('A',))
    B = threading.Thread(target=hello , args = ('B',))
    
    A.setDaemon(True)      #根据传入的布尔型参数设置线程是否是守护线程。
    B.setDaemon(True)      #可以令子线程在主线程结束时无条件跟随主线程一起结束。
    
    A.start()
    B.start()               #开启线程
    
    B.join(3)               #"监工"B线程3秒。
    
    print('线程A%s'%('还在工作中'if A.is_alive() else '已经结束工作！'))
    print('线程B%s'%('还在工作中'if B.is_alive() else '已经结束工作！'))
    
    
if __name__ =='__main__':
    demo()
    
    
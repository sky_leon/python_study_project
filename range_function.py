print(type(range(5)))

for i in range(5):
    print(i,end=',')

print('\n')

for i in range(5,10):             #在[5,10)区间生成序列，步长为1
    print(i,end=',')

print('\n')

for i in range(5,10,2):           #步长为2
    print(i,end=',')  
    
print('\n')             

    #序列生成器函数range()

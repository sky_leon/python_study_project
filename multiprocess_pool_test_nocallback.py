"""非阻塞式进程池"""
"""区别：
    (1)全部添加到队列中，立刻返回，并没有等待其他进程执行完毕；

    (2)回调函数等待进程任务完成才调用；

"""
from multiprocessing import Pool
import os
import time
from random import random
 
def task(task_name):
    print("开始做任务了！", task_name)
    start = time.time()
    # 休眠
    time.sleep(random())
    end = time.time()
    return "完成任务：{}!用时：{},进程id：{}".format(task_name, (end - start), os.getpid())
 

 
if __name__ == '__main__':
    pool = Pool(5)
    tasks = ["python1", "python2", "python3", "python4", "python5", "python6", "python7", "python8"]
    for task1 in tasks:
        pool.apply_async(task, args=(task1,))
 
    pool.close()
    pool.join()

 
    print("结束！！！")
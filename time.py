import time

def do_something():
    t0  = time.time()        #记录开始时间戳   精度较高的time.perf_counter()函数也可以
    for i in range(5):
        print('正在学习~~~~~')
        time.sleep(0.3)

    t1 = time.time()          #记录结束时间戳
    print('该函数所用总时间为%0.3f秒'%(t1-t0))


def do_something_test_2():
    t0  = time.time()        #记录开始时间戳   精度较高的time.perf_counter()函数用于实际工作时间很少的情况下。
    for i in range(3):
        print('正在学习~~~~~')
        time.sleep(0.0000001)

    t1 = time.time()          #记录结束时间戳
    print('该函数所用总时间为%0.3f秒'%(t1-t0))

def do_something_test():
    t0  = time.perf_counter()        #记录开始时间戳   精度较高的time.perf_counter()函数用于实际工作时间很少的情况下。
    for i in range(3):
        print('正在学习~~~~~')
        time.sleep(0.0000001)

    t1 = time.perf_counter()          #记录结束时间戳
    print('该函数所用总时间为%0.3f秒'%(t1-t0))
    

if __name__=='__main__':
    print(time.localtime())
    do_something()
    do_something_test()
    do_something_test_2()
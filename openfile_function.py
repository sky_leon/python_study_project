data = [[0.468,0.323,0.435],[0.5353,0.435,0.785]]

with open(r'd:\csv_data.csv','w') as fp:       #写入csv文件
    for line in data:
        ok = fp.write('%s\n'%','.join([str(item) for item in line]))


result = list()

with open(r'd:\csv_data.csv','r') as fp:       #读出csv文件并解析
    for line in fp.readlines():
        result.append([float(f) for f in line.strip().split(',')])

print(result)

#读写文件时，不管正常结束还是非正常结束，一定要关闭文件---通常需要捕获异常并进行处理。
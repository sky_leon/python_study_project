import cv2
from PIL import Image
import numpy as np

if __name__=='__main__':
    im_pil = Image.open(r'D:\python_project\images\pillow_test.jpg')     #打开图像文件
    print(im_pil.mode,im_pil.size)
    im_cv2 = np.array(im_pil)           #PIL图像格式转化为numpy数组

    cv2.imwrite(r'D:\python_project\images\pillow_test_pil_numpy.jpg',im_cv2[:,:,[2,1,0]]) 
    """
    cv2格式的图像是numpy数组，也就是numpy.ndarray对象。
    cv2格式图像的RGB模式，三个颜色通道的顺序是BGR，转换时需要交换R通道和B通道。
    """
    cv2.imshow('Image',im_cv2[:,:,[2,1,0]])
    cv2.waitKey(0)
    """
    cv2.waitKey(0)一个键盘绑定函数。其参数是以毫秒为单位的时间。该函数等待任何键盘事件指定的毫秒。如果您在这段时间内按下任何键，
    程序将继续运行。如果**0**被传递，它将无限期地等待一次敲击键。
    """
    cv2.destroyAllWindows()
    



    im_cv2 = cv2.imread(r'D:\python_project\images\pillow_test.jpg')
    im_pil = Image.fromarray(im_cv2[:,:,[2,1,0]])     #numpy数组转化为图像格式
    im_pil.show()
    im_pil.save(r'D:\python_project\images\pillow_test_pil.jpg')



nums = input('请输入3个整数，中间以空格分隔，按回车健结束输入：')
print(nums)
for item in nums.split():
    print(item)

    """
    input函数本身具备IO阻塞的功能，所以可以在程序中作为调试断点来使用。
    """

print(len([3,4,5]))
print(len('adsadada'))
print(len({'x':1,'y':2}))
print(len(range(5)))


    #可迭代对象长度函数len()


#特殊功能函数enumerate()/zip()/map()/chr()/ord()

for index,item in enumerate([True,False,None]):
    print(index , item,sep='->')                #sep设置分割符

for index,item in enumerate('xyz'):
    print(index,item,sep='->')


a = ['x','y','z']
b = [3,4,5]

for k,v in zip(a,b):                           #zip()函数可以将两个等长列表的对应元素组合成元组后返回一个迭代器。
    print(k,v,sep='->')           


def extract(x):    #开立方
    return pow(x, 1/3)

result = map(extract, [7,8,9])                 #map()函数。map(function ,可迭代的对象)。
print(list(result))

result = map(lambda x:pow(x,1/3), [7,8,9])
print(list(result))

print(chr(65))                             #chr()函数返回ASCII编码值对应的字符，ord()函数返回字符对应的ASCII编码值
print(ord('Z'))

for i in range(25):
    print(chr(65+i),sep='',end='')


print(divmod(5, 2))        #以元组 形式返回5/2的商和余数。
print(round(3.1415926))    #取整
print(round(3.1415926,5))  #精确到小数点后五位。


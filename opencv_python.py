import cv2

"""
opencv2当前以Numpy数组作为图像格式，同时可以使用Numpy数组强大的处理功能。
"""


if __name__=='__main__':
    im = cv2.imread(r'D:\python_project\images\pillow_test.jpg')
    print(im.shape)
    cv2.imshow('image',im)
    cv2.imwrite(r'D:\python_project\images\pillow_test_opencv.jpg',im)

    im_RGBA = cv2.imread(r'D:\python_project\images\pillow_test.jpg',cv2.IMREAD_UNCHANGED)  #改变为RGBA模式

    print(im_RGBA.shape)

    im_GRAY = cv2.imread(r'D:\python_project\images\pillow_test.jpg',cv2.IMREAD_GRAYSCALE) #改变为灰度模式
    print(im_GRAY.shape)
    cv2.imwrite(r'D:\python_project\images\pillow_opencv_gray.jpg',im_GRAY)

    cv2.waitKey(0)
    """
    cv2.waitKey(0)一个键盘绑定函数。其参数是以毫秒为单位的时间。该函数等待任何键盘事件指定的毫秒。如果您在这段时间内按下任何键，
    程序将继续运行。如果**0**被传递，它将无限期地等待一次敲击键。
    """
    cv2.destroyAllWindows()



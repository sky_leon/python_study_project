from PIL import Image , ImageDraw ,ImageGrab
from PIL import ImageFilter , ImageFont , ImageColor


if __name__=='__main__':
    im = Image.open(r'D:\python_project\images\pillow_test.jpg')     #打开图像文件
    print(im.mode,im.size)
    im.show()        #利用图像查看工具显示图像
    im_gray = im.convert('L')         #RGB模式转为L模式
    im_gray.save(r'D:\python_project\images\pillow_test_gray.jpg')    #保存图像

    r,g,b = im.split()           #将RGB图像拆分成独立的3个通道
    g.save(r'D:\python_project\images\pillow_test_green.jpg')       #保存绿色通道文件
    img_bgr = Image.merge('RGB',(b,g,r))       #交换红色、蓝色通道，得到特殊结果
    img_bgr.save(r'D:\python_project\images\pillow_test_BGR.jpg')   #保存交换通道后的图像
    img_bgr.show()

    im_30 = im.rotate(30)            #逆时针旋转30度，以原图分辨率返回图像。
    print(im_30.size)
    im_30.save(r'D:\python_project\images\pillow_test_rotate_30.jpg')

    im.rotate(30 ,expand=True).show()       #逆时针旋转30度，返回扩展的新图像。

    im_box = im.crop((150,50,400,200))       #裁切250*150的局部图像
    im_copy = im_box.copy()                  #复制局部图像
    im.paste(im_copy ,(350 ,450))            #粘贴到地图的右下角
    im.save(r'D:\python_project\images\pillow_test_copy_paste.jpg')


    im = Image.open(r'D:\python_project\images\pillow_test.jpg')     #打开图像文件

    im_detail = im.filter(ImageFilter.DETAIL)      #细节增强滤镜
    im_detail.show()
    im_detail.save(r'D:\python_project\images\pillow_test_detail.jpg')

    im_blur = im.filter(ImageFilter.BLUR)          #模糊滤镜
    im_blur.show()
    im_blur.save(r'D:\python_project\images\pillow_test_blur.jpg')

    im_contour = im.filter(ImageFilter.CONTOUR)    #轮廓滤镜
    im_contour.show()
    im_contour.save(r'D:\python_project\images\pillow_test_contour.jpg')

    im_edges = im.filter(ImageFilter.FIND_EDGES)   #勾画边界滤镜
    im_edges.show()
    im_edges.save(r'D:\python_project\images\pillow_test_edges.jpg')

    im_emboss = im.filter(ImageFilter.EMBOSS)       #浮雕滤镜
    im_emboss.show()
    im_emboss.save(r'D:\python_project\images\pillow_test_emboss.jpg')


    im_grab = ImageGrab.grab((1200,600,1920,1080))        #截取大小为720*480的屏幕区域
    im_grab.show()
    im_grab.save(r'D:\python_project\images\pillow_test_grab.jpg')

    
 
class A:

    static_x = 10             #定义静态变量
    def __init__(self):       #定义构造函数
        self.a = 10           #定义成员变量

    def getA(self):           #定义成员函数
        print('a = %d'%self.a)

    def __delattr__(self, name):
        print('执行析构函数，清理内存！')

if __name__=='__main__':
    a = A()                    #实例化
    a.getA()
    print(a.static_x)
    print(A.static_x)
    
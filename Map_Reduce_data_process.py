import time
from functools import reduce
import multiprocessing as mp

def power(x ,a = 2):
    """进程函数：幂函数

    Args:
        x ([type]): [description]
        a (int, optional): [description]. Defaults to 2.
    """
    time.sleep(0.5)      #延时0.5秒，模拟耗时的复杂计算
    return pow(x ,a)

if __name__ == '__main__':
    mp.freeze_support()
    print('开始计算.....')
    t0 = time.time()
    with mp.Pool(processes=8) as mpp:       #使用8进程并行计算。
        result_map = mpp.map(power,range(100))        #计算整数列表各元素的平方和。单进程运算至少耗时50秒。
        result = reduce(lambda result ,x :result+x ,result_map ,0)
        #result_map相当于把各个元素的平方和列出来。
        #1^2 + 2^2 +3^2 + 4^2 +........
        #result和x相当于形参。
        
    print('结果为%d，耗时%0.3f秒'%(result ,time.time() - t0))
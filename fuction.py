def bmi(height , weight ,name):
    i = weight/height**2
    print('%s的体重指数为%0.1f'%(name , i))

def bmi_variable_element(height , weight , *args , name = '您'):         #考虑使用体重均值来计算体重指数
    weight = (weight+sum(args))/(1+len(args))                     #  *arg为不确定个数的体重参数
    i = weight/height**2
    print('%s的体重指数为%0.1f'%(name , i))

def bmi_keyword_parameter(height , weight ,*args , name = '您',**kwds):  # **kwds是关键字参数，
    weight = (weight+sum(args))/(1+len(args))                     #  *arg为不确定个数的体重参数
    i = weight/height**2
    print('%s的体重指数为%0.1f'%(name , i))

    for key in kwds:                            #关键字参数由不限数量的键值对组成。在函数体内相当于一个字典。
        print('%s的%s是%s'%(name , key ,str(kwds[key])))

#参数的排列顺序是：位置参数排在首位，关键字参数排在末尾。

print('test__name__!')
print('__name__:',__name__)

if __name__=='__main__':
    bmi(1.75, 75, 'leon')
    bmi_variable_element(1.75, 75)
    bmi_variable_element(1.75, 75 , 74)
    bmi_variable_element(1.75, 75 , 74 ,75.5 , 74.7)
    bmi_variable_element(1.75, 75 , 74 ,75.5 , 74.7 ,name='leon')

    bmi_keyword_parameter(1.75, 75 , 74 ,name='leon' , 性别='male' , 爱好 = '看书！')
    print('__name__:',__name__)
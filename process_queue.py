#队列：进程间交换数据最常用的方法之一。
#Queue类的put()方法和get()方法默认为阻塞式，可以使用参数block指定为阻塞或非阻塞。

import os,time,random
import multiprocessing as mp

def sub_process_A(q):
    """A进程函数，生产数据：生产者。

    Args:
        q ([type]): [description]
    """
    while True:
        time.sleep(5*random.random())      #0~5秒的随机延迟
        q.put(random.randint(10 , 100))       #随机生成[10 ,100]的整数
        
def sub_process_B(q):
    """B进程函数，使用数据：消费者。

    Args:
        q ([type]): [description]
    """
    words =['哈哈','天哪！','My God!' ,'天上掉馅饼了！']
    while True:
        print('%s捡到了%d块钱！'%(words[random.randint(0,3)],q.get()))
        
if __name__ =='__main__':
    print('主线程(%s)开始 ，按任意键结束本程序！'%os.getpid())
    q = mp.Queue(10)
    
    p_a = mp.Process(target=sub_process_A , args=(q,))
    p_a.daemon = True      #设置子进程为守护进程
    p_a.start()
    
    p_b = mp.Process(target=sub_process_B , args=(q,))
    p_b.daemon =True
    p_b.start()
    
    input()      #利用input()函数阻塞主进程，常见的调试手段。
    
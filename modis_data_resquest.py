import requests
import json

sess = requests.Session()     #使用Session类的get()和post()方法，Session类在发送请求、接受应答时会自动处理cookie.

url_login = 'http://sdysit.com/modis'    #登录地址

form_login = {
    'account':'guest',
    'password':'hello'
}


resp = sess.post(url_login ,data=form_login)
print(resp.ok)         #查看应答是否成功


url_list = 'http://sdysit.com/modis/list'    #文件列表地址
resp = sess.get(url_list)
print(resp.ok)

result = json.loads(resp.text)     #解析json格式的应答
print(result['code'])       #应答代码为10，表示数据可用
print(result['data'])       #文件名和下载地址的列表


 
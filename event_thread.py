import time
import threading

E = threading.Event()    #创建事件

def work(id):
    """线程函数

    Args:
        id ([type]): [description]
    """
    print('<%d号员工>上班打卡！'%id)
    if E.is_set():            #已经到点了
        print('<%d号员工>迟到了！'%id)
        
    else:         #还没到点
        print('<%d号员工>看新闻中....'%id)
        E.wait()      #等待上班铃声
        
    print('<%d号员工>开始工作了....'%id)
    time.sleep(10)     #工作时间为10秒
    print('<%d号员工下班了....'%id)
    
def demo():
    
    E.clear()      #设置为"未到上班时间"
    threads = list()
    
    for i in range(3):        #3人提前上班打卡。
        threads.append(threading.Thread(target = work , args =(i ,)))
        threads[-1].start()
        
    time.sleep(5)     #5秒后上班时间到
    E.set()
    
    time.sleep(5)     #5秒后，老板到了。
    threads.append(threading.Thread(target = work , args =(9, )))
    threads[-1].start()
    
    for t in threads:
        t.join()
        
    print('都下班了，关灯！')
    
if __name__ =='__main__':
    demo()
        
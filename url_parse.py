import urllib3

http = urllib3.PoolManager()

resp = http.request('GET','https://cn.bing.com')      #发送GET请求，每个请求都会返回一个response应答对象

print('HTTP协议状态码：%d\n' %(resp.status))

print('应答报头中的Content-Type:%s\n' %(resp.headers))

html = resp.read()       #取得应答内容(body内容)

print(len(html))
print(type(html))
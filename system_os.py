import os
import time
print(os.getcwd())         #返回程序当前路径
print(os.listdir(r'd:\python_project'))      #返回指定路径下的文件夹和文件列表
os.mkdir(r'd:\python_project\temp')       #新建单级文件(新文件夹的父级已存在)
print(os.listdir(r'd:\python_project'))

os.makedirs(r'd:\python_project\temp\A\B')     #新建多级文件夹
os.rmdir(r'd:\python_project\temp\A\B')         #删除空文件夹
os.rmdir(r'd:\python_project\temp\A')

os.path.isfile(r'd:\python_project\assert.py')    #判断是否是文件
os.path.exists(r'd:\python_project\assert.py')    #判断文件是否存在
os.path.exists(r'd:\python_project\temp\A\B')     #判断文件夹是否存在

file_info = os.stat(r'd:\python_project\assert.py')  #返回文件的大小、创建时间等信息
print('文件大小：%d' %(file_info.st_size))    #文件大小(以字节为单位)
print('文件创建时间：%f' %(file_info.st_ctime))     #文件创建时间(时间戳)
print('文件最后更新时间：%f' %(file_info.st_mtime))    #文件最后更新时间(时间戳)
print('文件最后访问时间：%f' %(file_info.st_atime))     #文件最后访问时间(时间戳)

print(time.strftime("%Y-%m-%d %X" ,time.localtime(file_info.st_ctime)))
print(time.strftime("%Y-%m-%d %X" ,time.localtime(file_info.st_mtime)))
print(time.strftime("%Y-%m-%d %X" ,time.localtime(file_info.st_atime)))

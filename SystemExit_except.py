import sys
import time
 
print("Type exit to exit.")
while True:
    response = input("Your favorite sports: ")
    if response.upper() == "EXIT":
        print("You've typed exit, and system will exit right now...")
        time.sleep(0.1)
        sys.exit()  # 程序直接退出，不捕捉异常
    print('You typed ' + response + '.')
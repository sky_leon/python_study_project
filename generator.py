def get_square(n):
    for i in range(n):
        yield(pow(i, 2))         
#yield是一个类似return 的关键字，迭代一次遇到yield的时候就返回yield后面或者右面的值。而且下一次迭代的时候，从上一次迭代遇到的yield后面的代码开始执行。
#生成器（generator）能够迭代的关键是他有next()方法，工作原理就是通过重复调用next()方法，直到捕获一个异常。
#带有yield的函数不再是一个普通的函数，而是一个生成器generator，可用于迭代
def fib(max):

    n,a,b =0,0,1

    while n < max:

        yield b

        a,b =b,a+b

        n = n+1

    return 'done'

lis = [x*x for x in range(5)]


if __name__=='__main__':
    grator = get_square(5)
    print(grator)
    print(lis)
    for i in grator:               #使用生成器表达式可以取代列表解析并且可以同时节省内存。
        print(i,end=',')
    
    for i in fib(6):
        print(i)
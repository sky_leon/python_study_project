import numpy as np
import cv2

im = np.zeros((300 ,800 ,3) , dtype = np.uint8)        #生成800*300的黑色背景图

im = cv2.line(im , (0 ,200) ,(800 ,200) ,(0 , 0 ,255) ,2)    #画线
im = cv2.rectangle(im ,(20 ,20) ,(180 ,180 ),(255 ,0 ,0) ,1)  #画矩形
im = cv2.circle(im , (320 , 100) ,80 ,(0 , 255 ,0) , -1)     #画圆

font = cv2.FONT_HERSHEY_SIMPLEX

im = cv2.putText(im , 'hello world.' ,(420 , 100) ,font ,2 ,(255 ,255 ,255 ) , 2 ,cv2.LINE_AA)
cv2.imshow('Image!',im)
cv2.waitKey(0)
cv2.destroyAllWindows()

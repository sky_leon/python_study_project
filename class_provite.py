class A:
    def __init__(self , a ,b,c):
        self.a = a   #共有属性
        self._b = b  #保护属性
        self.__c = c  #私有属性

    def x(self):
        print('公有方法！')

    def _y(self):
        print('保护方法！')
    
    def __z(self):
        print('私有方法！')   #私有属性和方法的外部访问受到限制。


if __name__=='__main__':
    a = A(1 ,2 ,3)
    print(a.a)
    print(a._b)
    a.x()
    a._y()

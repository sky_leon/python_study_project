import os,time,random
import queue
import threading

    
def sub_thread_A(q):
    """A线程函数：生产数据->生产者

    Args:
        q ([type]): [description]
    """
    
    while True:
        time.sleep(5*random.random())      #0~5秒的随机延时
        q.put(random.randint(10, 100))      #随机生成[10,100]的整数
        
def sub_thread_B(q):
    """B线程函数：使用数据->消费者

    Args:
        q ([type]): [description]
    """
    
    words = ['哈哈' , '天哪！' ,'My god!' , '天上掉馅饼了！']
    while True:
        print('%s捡到了%d块钱！'%(words[random.randint(0, 3)] , q.get()))
        
        
if __name__ =='__main__':
    print('线程(%s)开始，按回车键结束本程序'%os.getpid())
    
    q = queue.Queue(10)         #创建队列
    
    A = threading.Thread(target=sub_thread_A , args=(q ,))
    A.setDaemon(True)
    A.start()
    
    B = threading.Thread(target=sub_thread_B , args=(q ,))
    B.setDaemon(True)
    B.start()
    
    input()        #利用input()函数阻塞主线程，常用的调试手段。 
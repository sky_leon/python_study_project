Y,M,D,h,m,s = 2021,4,26,20,16,0

print('{:04d}-{:02d}-{:02d} {:02d}:{:02d}:{:02d}'.format(Y,M,D,h,m,s))
print('%04d-%02d-%02d %02d:%02d:%02d'%(Y,M,D,h,m,s))


a = [[6,5],[3,7],[2,8]]
print(sorted(a,key=lambda x:x[0]))        #根据每一行的首元素排序
print(sorted(a,key=lambda x:x[-1]))       #根据每一行的尾元素排序


#lambda表达式，又称匿名函数，常用来表示内部仅含一行表达式的函数。
#语法格式为：name = lambda[list]
"""lambda表达式的转换格式：
def name(list):
    return 表达式
name(list)
"""

b = [{'name':'c','age':18},{'name':'A','age':20},{'name':'B','age':19}]
print(sorted(b,key=lambda x:x['name']))            #根据name键排序
print(sorted(b,key=lambda x:x['age']))             #根据age键排序
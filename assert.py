import time
def i_want_to_sleep(delay):            #断言就是声明表达式的布尔值必须为真的判定。是调试手段，一般不在生产环境中使用。
    assert(isinstance(delay, (int , float)))   #函数参数必须为整数或者浮点数。
    print('sleeping!')
    time.sleep(delay)
    print('睡醒了！')

if __name__=='__main__':
    i_want_to_sleep(1.2)
    i_want_to_sleep(2)
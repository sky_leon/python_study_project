import time
import threading

S = threading.Semaphore(5)      #表示有5吧电锤可供使用。信号量的初始值->当前可用的资源数。

def us_hammer(id):
    """线程函数

    Args:
        id ([type]): [description]
    """
    
    S.acquire()   #P操作，阻塞式请求电锤。申请信号量资源。
    time.sleep(0.2)
    print('%d号刚刚用完电锤'%id)
    S.release()   #V操作，释放资源（信号量加1）
    
def demo():
    threads = list()
    for i in range(30):       #30个工人请求电锤
        threads.append(threading.Thread(target = us_hammer , args= (i ,))) #将线程函数分配给线程完成任务。
        threads[-1].start()
        
    for t in threads:
        t.join()
        
    print('所以线程工作结束！')
    
if __name__ =='__main__':
    demo()

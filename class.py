class Animal:
    def eat(self):
        print('我能吃东西！')

class Fash(Animal):    #单继承
    def __init__(self , name):
        self.name = name
    def swim(self):
        print('我会游泳！')


class Bird(Animal):    #单继承
    def __init__(self , name):
        self.name = name

    def who(self):
        print('我是%s'%self.name)

    def fly(self):
        print('我会飞！')

class Batman(Bird):     #单继承,显示地调用父类的构造函数
    def __init__(self, name , color):
        Bird.__init__(self ,name)
        self.color = color
    def say(self): 
        print('我会说话，喜欢%s'%self.color)   


class ULtraman(Fash , Batman):    #多继承
    def __init__(self, name , color , region):
        super(ULtraman, self).__init__(name)
        super(Fash, self).__init__(name , color)
        self.region = region

    def where(self):
        print('我来自%s'%self.region)


if __name__=='__main__':
    uman = ULtraman('迪迦奥特曼', 'red', '宇宙')
    uman.who()
    uman.where()
    uman.say()
    uman.eat()
    uman.fly()
    uman.swim()
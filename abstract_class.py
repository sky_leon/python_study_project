import abc

class A(object , metaclass = abc.ABCMeta):               #定义抽象类，定义两个成员函数
    @abc.abstractmethod
    def f1(self):
        pass

    @abc.abstractmethod
    def f2(self):
        pass



class B(A):            #继承抽象类，重写了两个成员函数
    def f1(self):
        print('重写f1')

    def f2(self):
        print('重写f2')

class C(A):             #只重写了一个成员函数
    def f1(self):
        print('重写f1')

if __name__=='__main__':
    b = B()
    b.f1()
    b.f2()
    
    
